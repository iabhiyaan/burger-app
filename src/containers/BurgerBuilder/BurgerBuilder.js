import React, { Component } from "react";
import Aux from "../../hoc/Aux/Aux";
import Burger from "../../components/Burger/Burger";
import BuildControls from "../../components/Burger/BuildControls/BuildControls";
import Modal from "../../components/UI/Modal/Modal";
import OrderSummary from "../../components/Burger/OrderSummary/OrderSummary";
import axios from "../../axiosOrder";
import Spinner from "../../components/UI/Spinner/Spinner";
// import spinner from "../../../Spinner/Spinner";
const all__prices = {
	salad: 0.4,
	bacon: 0.9,
	meat: 1.4,
	cheese: 1
};
class BurgerBuilder extends Component {
	state = {
		ingredients: {
			salad: 0,
			bacon: 0,
			meat: 0,
			cheese: 0
		},
		totalPrice: 4,
		order: false,
		sum: null,
		purchasing: false,
		loading: false
	};
	updateCart = ingredients => {
		// const ingredients = {...this.state.ingredients}
		const sum = Object.keys(ingredients)
			.map(igKey => ingredients[igKey])
			.reduce((s, el) => s + el, 0);
		this.setState({
			order: sum > 0,
			sum
		});
	};
	addItems = type => {
		const oldCount = this.state.ingredients[type];
		const updatedCount = oldCount + 1;
		const updatedItems = { ...this.state.ingredients };
		updatedItems[type] = updatedCount;
		const getallPrice = all__prices[type];
		const OldPrice = this.state.totalPrice;
		const addedPrice = OldPrice + getallPrice;
		this.setState({
			totalPrice: addedPrice,
			ingredients: updatedItems
		});
		this.updateCart(updatedItems);
	};
	removeItems = type => {
		const oldCount = this.state.ingredients[type];
		if (oldCount <= 0) {
			return;
		}
		const removeItems = oldCount - 1;
		const updatedItems = { ...this.state.ingredients };
		updatedItems[type] = removeItems;
		const getallPrice = all__prices[type];
		const removePrice = this.state.totalPrice;
		const updatedPrice = removePrice - getallPrice;
		this.setState({
			ingredients: updatedItems,
			totalPrice: updatedPrice
		});
		this.updateCart(updatedItems);
	};
	purchaseHandler = e => {
		this.setState({
			purchasing: true
		});
	};
	purchaseCancler = e => {
		this.setState({
			purchasing: false
		});
	};
	purchaseContinue = e => {
		// alert('You can continue');
		this.setState({
			loading: true
		})
		const orders = {
			ingredients: this.state.ingredients,
			price: this.state.totalPrice.toFixed(2),
			customer: {
				name: "Abhiyan Shrestha",
				address: {
					street: "Sungava",
					zipCode: "44600",
					country: "Nepal"
				},
				email: "abhiyaan@yahoo.com"
			},
			delieveryMethod: "Fastest"
		};
		axios
			.post("/orders.json", orders)
			.then(res => this.setState({
				loading: false,
				purchasing: false
			}))
			.catch(err => this.setState({
				loading: false,
				purchasing: false
			}));
	};
	render() {
		const disabledInfo = { ...this.state.ingredients };
		for (let key in disabledInfo) {
			disabledInfo[key] = disabledInfo[key] <= 0;
		}
		let orderSummary = (
			<OrderSummary
				totalPrice={this.state.totalPrice}
				ingredients={this.state.ingredients}
				purchaseContinued={this.purchaseContinue}
				purchaseCancler={this.purchaseCancler}
			/>
		);
		if (this.state.loading) {
			orderSummary = <Spinner />
		}
		return (
			<Aux>
				{/* <h1> {this.state.sum} </h1> */}
				<Modal show={this.state.purchasing} modalClosed={this.purchaseCancler}>
					{orderSummary}
				</Modal>

				<Burger ingredients={this.state.ingredients} />
				<BuildControls
					addItems={this.addItems}
					removeItems={this.removeItems}
					disabled={disabledInfo}
					totalPrice={this.state.totalPrice}
					cart={this.state.order}
					ordered={this.purchaseHandler}
				/>
			</Aux>
		);
	}
}

export default BurgerBuilder;
