import React from "react";
import Aux from "../Aux/Aux";
import classes from "./Layout.css";
import Toolbar from "../../components/Navigation/Toolbar/Toolbar";
import SideDrawer from "../../components/Navigation/SideDrawer/SideDrawer";

class Layout extends React.Component {
	state = {
		showSideDrawer: false
	};
	showSideDrawerHandler = () => {
		this.setState({
			showSideDrawer: false
		});
	};
	showSideDrawerToggleHandler = () => {
		this.setState(prevState => ({ showSideDrawer: !prevState.showSideDrawer }));
	};
	render() {
		return (
			<Aux>
				{/* <div>Toolbar, Sidebar</div> */}
				<Toolbar drawerToggleClicked = {this.showSideDrawerToggleHandler} />
				<SideDrawer open={this.state.showSideDrawer} closed={this.showSideDrawerHandler} />
				<main className={classes.mt__4}>{this.props.children}</main>
			</Aux>
		);
	}
}

export default Layout;
