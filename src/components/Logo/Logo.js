import React from "react";
import burgerLogo from "../../assets/images/b.png";
import classes from "./Logo.css";
const Logo = props => (
	<div className={classes.Logo} style={{ height: props.height }}>
		<img src={burgerLogo} alt="bbb" />
	</div>
);
export default Logo;
