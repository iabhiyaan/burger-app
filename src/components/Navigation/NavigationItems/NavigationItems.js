import React from "react";
import classes from "./NavigationItems.css";
import NavigationItem from "./NavigationItem/NavigationItem";
const NavigationItems = props => (
	<div>
		<ul className={classes.NavigationItems}>
			<NavigationItem link="/" active >Burger Builder</NavigationItem>
			<NavigationItem link="/">Check Out</NavigationItem>
		</ul>
	</div>
);
export default NavigationItems;
