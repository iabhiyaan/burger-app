import React from "react";
import classes from "./Backdrop.css";
// import Aux from "../../../hoc/Aux/Aux";
const Backdrop = props => props.show && <div className={classes.Backdrop} onClick={props.clicked} />;
export default Backdrop;
