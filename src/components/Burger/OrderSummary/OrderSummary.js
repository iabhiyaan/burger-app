import React from "react";
import Aux from "../../../hoc/Aux/Aux";
import Button from '../../UI/Button/Button';
const OrderSummary = props => {
	const summary = Object.keys(props.ingredients).map(igKey => {
		return (
			<li key={igKey}>
				<span className={{ textTransform: "capitalize" }}> {igKey}: </span> {props.ingredients[igKey]}
			</li>
		)
	});
	return (
		<Aux>
			<h3>Your Order</h3>
			<p>A delicious burger with following ingredients:</p>
			<ul>{summary}</ul>
			<p> Continue to checkout? </p>
			<p> <strong> Total Price: { props.totalPrice.toFixed(2) } </strong> </p>
			<Button btnType="Danger" clicked={props.purchaseCancler}>CANCEL</Button>
			<Button btnType="Success" clicked={props.purchaseContinued}>CONTINUE</Button>
		</Aux>
	);
};

export default OrderSummary;
