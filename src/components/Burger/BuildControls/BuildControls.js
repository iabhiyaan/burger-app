import React from "react";
import classes from "./BuildControls.css";
import BuildControl from "./BuildControl/BuildControl";
const controls = [
	{ label: "Salad", type: "salad" },
	{ label: "Cheese", type: "cheese" },
	{ label: "Meat", type: "meat" },
	{ label: "Bacon", type: "bacon" }
];
const BuildControls = props => (
	<div className={classes.BuildControls}>
		<p>
			Current Price : <strong> {props.totalPrice.toFixed(2)} </strong>
		</p>
		{controls.map(ctrl => (
			<BuildControl
				addItems={() => props.addItems(ctrl.type)}
				removeItems={() => props.removeItems(ctrl.type)}
				disabled={props.disabled[ctrl.type]}
				key={ctrl.label}
				label={ctrl.label}
			/>
		))}
		<button disabled={!props.cart} className={classes.OrderButton} onClick={props.ordered}>ORDER NOW</button>
	</div>
);

export default BuildControls;
