import React from "react";

import classes from "./BuildControl.css";

const BuildControl = ({ label,addItems,removeItems,disabled,totalPrice }) => (
	<div className={classes.BuildControl}>
		<div className={classes.Label}> {label} </div>
        <button className={classes.Less} onClick = {removeItems} disabled = {disabled}> Less </button>
        <button className={classes.More} onClick = {addItems}> More </button>
	</div>
);

export default BuildControl;
