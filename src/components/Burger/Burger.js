import React from "react";
import classes from "./Burger.css";
import BurgerIngredient from "../Burger/BurgerIngredient/BurgerIngredient";
const Burger = ({ ingredients }) => {
	let tIg = Object.keys(ingredients)
	.map(iKey =>
		[...Array(ingredients[iKey])]
			.map((_, i) => <BurgerIngredient key={i} type={iKey} />)
	)
	.reduce((arr, el) => arr.concat(el), []);
	console.log(tIg.length);   
	if (tIg.length === 0) {
		// console.log(`Items is ${tIg.length}.Please add items`);
		tIg = <p>Please add Items</p>
	}
	return (
		<div className={classes.Burger}>
			<BurgerIngredient type="bread-top" />
			{ tIg && tIg}
			<BurgerIngredient type="bread-bottom" />
		</div>
	);
};
export default Burger;
